angular.module('app.utils', [])

.factory('$localStorage', ['$window', function($window) {
	  return {
	    set: function(key, value) {
	      $window.localStorage[key] = value;
	    },
	    get: function(key, defaultValue) {
	      return $window.localStorage[key] || defaultValue;
	    },
	    setObject: function(key, value) {
	      $window.localStorage[key] = JSON.stringify(value);
	    },
	    
	    getObject: function(key) {
	    	var parse = JSON.parse($window.localStorage[key] || '{}');
	    	return parse;
	    }
	  }
	}]);