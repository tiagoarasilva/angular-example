angular.module('routes', ['ui.router', 'ngSanitize', 'app.controllers', 'app.services', 'app.routes', 
                          'app.configs', 'app.utils']);