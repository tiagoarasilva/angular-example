angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {
	
	
	$stateProvider
	
	.state('dash', {
		url: '/dash',
		templateUrl: "templates/dashboard.html",
		controller: 'DashCtrl'
	})
	
	
	.state('upload', {
		url: '/upload',
		templateUrl: "templates/upload.html",
		controller: 'UploadCtrl'
	})
	
	.state('library', {
		url: '/library',
		templateUrl: "templates/library.html",
		controller: 'LibraryCtrl'
	})
	
	.state('about', {
		url: '/about',
		templateUrl: "templates/about.html",
		controller: 'AboutCtrl'
	})
	
	
	
	/*
	 * Search 
	 * */
	.state('search', {
		url: '/search',
		templateUrl: "templates/search.html",
		controller: 'SearchCtrl'
	})
	
	.state('result', {
		url: '/result',
		templateUrl: "templates/components/search/result.html",
		controller: 'SearchResultCtrl'
	})
	
	/*.state('search.result', {
		url: '/result',
		views: {
			'result' : {
				templateUrl: 'templates/components/search/result.html',
				controller: 'SearchCtrl'
			}
		}
	})*/

	
	
	
/*	multiple views
	.state('dash', {
		url: '/dash',
		
		views: {
			'' : {
				templateUrl: "templates/dashboard.html"
			},
			
			'spinner@dash' : {
				templateUrl: 'templates/components/spinner.html',
				controller: 'DashCtrl'
			},
			
			'table@dash' : {
				templateUrl: 'templates/components/table.html',
				controller: 'DashCtrl'
			}
		}
//		templateUrl: "templates/dashboard.html",
//		controller: 'DashCtrl'
	})*/
	
	
	// in case of failure
	$urlRouterProvider.otherwise('/dash');
});