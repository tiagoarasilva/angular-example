angular.module('app.controllers', [])

.controller('GeneralCtrl', function($scope, $state) {
	
	$scope.year = Date.now();
	$scope.title = "TBS-Connect";
    
    // selected tab
	this.tab = 1;
	
	$scope.GoToState = function(page) {
		$state.go(page);
	}
	/*
	 * Manipulate Session
	 * */
	$scope.showLogout = false;
	
	/*
	 * functions to show the active tab
	 * */
	this.selectTab = function(setTab) {
		this.tab = setTab;
	}
	
	this.isSelected = function(checkTab) {
		return this.tab === checkTab;
	}
	
})

.controller('DashCtrl', function($scope) {
	
	$scope.title = "Dashboard";
	$scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];
})

.controller('SearchCtrl', function($scope, $rootScope, $state, SearchService) {
	
	// local variables
	var root = $rootScope;
	
	// elements of the page
	$scope.info = {};
	$scope.title = "Search";
	
	// Search for the results 
	$scope.search = function() {
		SearchService.Search().then(function(value) {
			$scope.resultSet = value.data.searchResults.list.data;
			
//			broadcast result to result page
//			root.$broadcast('result', $scope.resultSet);
			$state.go('result');

		}, function(reason) {
			
		})
	}
})

.controller('SearchResultCtrl', function($scope, $rootScope, SearchService) {
	
	$scope.title = "Result";
	var root = $rootScope;
	
//	result received from the $emmit/broadcast
//	root.$on('result', function(event, data) {
//		$scope.resultSet = angular.copy(data);
//	});
	
	$scope.resultSet = SearchService.getResults();
	
	
	
})

.controller('UploadCtrl', function($scope) {
	$scope.title = "Upload";
})


.controller('LibraryCtrl', function($scope) {
	$scope.title = "Library";
})

.controller('AboutCtrl', function($scope) {
	$scope.title = "About";
	
});